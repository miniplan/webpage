import {DOWNLOAD_URL} from "../../static";
import {GradientButton} from "../../components/GradientButton";
import {TextButton} from "../../components/TextButton";
import {appRoute, privacyRoute} from "../../routes";
import Card from "../../util/Card";
import {A} from "@solidjs/router";

export default function Home() {
    return (
        <Card>
            <h1 class="text-lg font-semibold">MiniPlan</h1>
            <h2 class="mb-3">Digitaler MiniPlan</h2>

            <div class="flex space-x-1">
                <a href={appRoute.path}>
                    <TextButton>Zur Web-App</TextButton>
                </a>
                <A href={DOWNLOAD_URL}>
                    <GradientButton text="MiniPlan-App herunterladen"/>
                </A>
            </div>
        </Card>
    );
}

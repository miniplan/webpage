import {createResource, Resource} from "solid-js";
import {API_HOST} from "../../static";
import {useParams} from "@solidjs/router";

export type InviteData = {
    exists: boolean,
    token: string
}

const fetchStatus = async (token: string): Promise<InviteData> => {
    const request = await fetch(`${API_HOST}/check/${token}`);

    if (request.status === 200) {
        const json = await request.json();
        return {
            exists: json["exists"],
            token: token
        };
    }

    return {
        exists: false,
        token: null
    };
};

export default function InviteData(): Resource<InviteData> {
    const params = useParams();
    const [data] = createResource<InviteData, string>(() => params.token, fetchStatus);

    return data;
}

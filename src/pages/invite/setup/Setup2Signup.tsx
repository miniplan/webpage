import {InferType, object, string} from "yup";
import {useSignupContext} from "../../../util/SignupProvider";
import {createForm} from "@felte/solid";
import {validator} from "@felte/validator-yup";
import {FormControl, FormLabel, HStack, Input, VStack} from "@hope-ui/solid";
import {GradientButton} from "../../../components/GradientButton";
import {createSignal, Show} from "solid-js";
import {ErrorIcon} from "../../../components/ErrorIcon";
import {ArrowRightIcon} from "../../../components/ArrowRightIcon";
import {next} from "../../../util/PageUtil";
import {privacyRoute} from "../../../routes";
import {A, useSearchParams} from "@solidjs/router";

const registrationSchema = object({
    firstname: string().required(),
    lastname: string().required(),
    // phoneNumber: string().required().test((number) => isValidPhoneNumber(number))
});

export function Setup2Signup() {
    const [errorText, setErrorText] = createSignal(null);
    const [state, {updateFirstname, updateLastname}] = useSignupContext();
    const [searchParams, setSearchParams] = useSearchParams();

    const {form, errors, isValid,} = createForm<InferType<typeof registrationSchema>>({
        extend: validator({schema: registrationSchema}),
        onSubmit: (values) => {
            updateFirstname(values.firstname);
            updateLastname(values.lastname);

            setSearchParams(next(searchParams));
        },
    });

    return <div>
        <h1 class="text-lg font-semibold">Willkommen bei MiniPlan!</h1>
        <h2 class="text-pink-500 text-xs font-semibold mb-3">Fülle die folgenden Felder aus, um deinen Account zu
            erstellen</h2>

        <Show when={errorText() != null}>
            <div class="flex justify-content items-center text-red-600 space-x-1 mb-2">
                <ErrorIcon/>
                <p class="text-red-600 text-xs font-semibold">{errorText()}</p>
            </div>
        </Show>

        <VStack as="form" ref={form} spacing="$5" alignItems="stretch" maxW="$96" mx="auto">
            <FormControl required invalid={!!errors("firstname")}>
                <FormLabel>Vorname</FormLabel>
                <Input type="text" name="firstname" placeholder="Max"/>
            </FormControl>

            <FormControl required invalid={!!errors("lastname")} onfocus={() => alert("test")}>
                <FormLabel>Nachname</FormLabel>
                <Input type="text" name="lastname" placeholder="Mustermann"/>
            </FormControl>

            <h3 class="text-xs text-gray-600">Mit der Registration stimmst du der <A href={privacyRoute.path}><span
                class="underline">Datenschutzerklärung</span></A> zu.</h3>

            <HStack justifyContent="flex-end">
                <GradientButton type="submit" disabled={!isValid()} iconRight={true} icon={<ArrowRightIcon/>}
                                text="Weiter"/>
            </HStack>
        </VStack>
    </div>;
}

import {useSignupContext} from "../../../util/SignupProvider";
import {GradientButton} from "../../../components/GradientButton";
import {ArrowRightIcon} from "../../../components/ArrowRightIcon";
import {next} from "../../../util/PageUtil";
import {useSearchParams} from "@solidjs/router";

export function Setup1Explainer() {
    const [_, {incrementUiFrame}] = useSignupContext();
    const [searchParams, setSearchParams] = useSearchParams();


    return (<div>
        <img src="/mini.webp" alt="mini icon"/>

        <h1 class="text-lg font-semibold mt-2">Willkommen bei MiniPlan!</h1>
        <h2 class="text-pink-500 font-semibold">Du wurdest zu MiniPlan eingeladen!</h2>

        <div class="mt-2 flex justify-start">
            <GradientButton text="Weiter" iconRight={true} icon={<ArrowRightIcon/>}
                            onClick={() => setSearchParams(next(searchParams))}/>
        </div>
    </div>);
}

import {API_HOST} from "../../../static";

export async function sendCode(token: string, phoneNumber: string) {
    return await fetch(`${API_HOST}/signup`, {
        method: "POST",
        body: JSON.stringify({inviteToken: token, phoneNumber: phoneNumber})
    })
}

import {InferType, number, object, string} from "yup";
import {useSignupContext} from "../../../util/SignupProvider";
import {createForm} from "@felte/solid";
import {validator} from "@felte/validator-yup";
import {FormControl, FormLabel, HStack, Input, InputGroup, InputLeftAddon, VStack} from "@hope-ui/solid";
import {GradientButton} from "../../../components/GradientButton";
import CheckmarkIcon from "../../../components/CheckmarkIcon";
import {API_HOST} from "../../../static";
import {createSignal, Show} from "solid-js";
import {isValidPhoneNumber} from "libphonenumber-js";
import {SimpleTextButton} from "../../../components/SimpleTextButton";
import {ErrorIcon} from "../../../components/ErrorIcon";
import {GreenSuccessButton} from "../../../components/GreenSuccessButton";
import {errors} from "../../../util/errorCodes";
import {TextButton} from "../../../components/TextButton";
import {next, previous} from "../../../util/PageUtil";
import {sendCode} from "./codeUtil";
import {useNavigate, useSearchParams} from "@solidjs/router";

const phoneNumberSchema = object({
    phoneNumber: string().required().test((number) => isValidPhoneNumber(number, "AT"))
});

const otpSchema = object({
    otp: number().required()
});

export function Setup3Otp() {
    const [state, {incrementUiFrame, updatePhoneNumber, updateLoginRedisToken}] = useSignupContext();
    const [counter, setCounter] = createSignal(60);
    const [phoneErrorText, setPhoneErrorText] = createSignal(null);
    const [userCreated, setUserCreated] = createSignal(false)

    const [searchParams, setSearchParams] = useSearchParams();
    const navigate = useNavigate();

    if (state.lastname == null || state.firstname == null) {
        navigate("?page=1")
    }

    function setupCounter() {
        const interval = setInterval(() => {
            if (counter() > 0) {
                setCounter(counter() - 1)
            } else {
                clearInterval(interval);
            }
        }, 1100);
    }

    const {
        form: phoneForm,
        errors: phoneErrors,
        isValid: phoneIsValid
    } = createForm<InferType<typeof phoneNumberSchema>>({
        extend: validator({schema: phoneNumberSchema}),
        onSubmit: async (values) => {
            const validPhone = isValidPhoneNumber(values.phoneNumber, "AT")
            if (!validPhone) {
                setPhoneErrorText("Ungültige Handynummer!")
                return
            }

            updatePhoneNumber(values.phoneNumber);
            const codeRequest = await sendCode(state.token, values.phoneNumber);
            if (codeRequest.status !== 200) {
                const error = await codeRequest.text();
                setPhoneErrorText(errors[error] ?? error);
                return
            }

            setUserCreated(true);
        }
    })

    const {form: codeForm, errors: codeErrors, isValid: codeIsValid} = createForm<InferType<typeof otpSchema>>({
        extend: validator({schema: otpSchema}),
        onSubmit: async (values) => {
            const signupRequest = await fetch(`${API_HOST}/signup/confirm`, {
                method: "POST",
                body: JSON.stringify({
                    invite: state.token,
                    phoneNumber: state.phoneNumber,
                    otp: values.otp,
                    firstname: state.firstname,
                    lastname: state.lastname,
                })
            });

            if (signupRequest.status !== 200) {
                const error = await signupRequest.text();
                setPhoneErrorText(errors[error] ?? error);
                return
            }

            updateLoginRedisToken((await signupRequest.json())["key"]);
            setSearchParams(next(searchParams));
        },
    });

    let phoneNumberRef = null;

    async function retryClicked() {
        await sendCode(state.token, phoneNumberRef.value);
        setCounter(60);
        setupCounter();
    }

    function resetError() {
        setPhoneErrorText(null);
    }

    return (
        <div>
            <h1 class="text-lg font-semibold">Willkommen bei MiniPlan!</h1>
            <h2 class="text-pink-500 text-xs font-semibold mb-3">Gib deine Handynummer ein, damit wir dir einen
                Einmalcode schicken können</h2>

            <Show when={phoneErrorText() !== null}>
                <div class="flex justify-content items-center text-red-600 space-x-1 mb-2">
                    <ErrorIcon/>
                    <p class="text-red-600 text-xs font-semibold">{phoneErrorText()}</p>
                </div>
            </Show>

            <VStack as="form" ref={phoneForm} spacing="$5" alignItems="stretch" maxW="$96" mx="auto">
                <VStack>
                    <FormControl required invalid={!!phoneErrors("phoneNumber")}>
                        <FormLabel>Handynummer</FormLabel>
                        <InputGroup>
                            <InputLeftAddon>+43</InputLeftAddon>
                            <Input type="tel" name="phoneNumber" ref={phoneNumberRef} placeholder="123 456789"
                                   pattern="[0-9]*"
                                   oninput={(e) => {
                                       // @ts-ignore
                                       // e.target.value = format(e.target.value)
                                   }} onfocus={resetError}/>
                        </InputGroup>
                    </FormControl>
                </VStack>

                <HStack justifyContent="flex-end">
                    <Show when={!userCreated()}
                          fallback={<GreenSuccessButton iconRight={true} icon={CheckmarkIcon} text="Code gesendet"
                                                        onClick={() => {
                                                        }}/>}>
                        <GradientButton text="Code senden" disabled={!phoneIsValid()} onClick={() => setupCounter()}/>
                    </Show>
                </HStack>
            </VStack>

            <VStack as="form" ref={codeForm} spacing="$5" alignItems="stretch" maxW="$96" mx="auto">
                <FormControl required invalid={!!codeErrors("otp")} disabled={!phoneIsValid()}
                    // @ts-ignore
                             disabled:class="cursor-default">
                    <FormLabel>Einmalcode</FormLabel>
                    <Input type="number" name="otp" min="000000" max="999999" placeholder="123456"/>
                </FormControl>

                <HStack>
                    <SimpleTextButton onClick={() => {
                        retryClicked()
                    }} disabled={counter() > 0}>Code erneut anforden<Show
                        when={counter() > 0}><span> (in {counter()} Sekunden)</span></Show></SimpleTextButton>
                </HStack>

                <HStack spacing="$1" justifyContent="flex-end" className={"text-gray-400"}>
                    <TextButton onClick={() => setSearchParams(previous(searchParams))}>Zurück</TextButton>

                    <GradientButton type="submit" disabled={!codeIsValid()} iconRight={true} icon={<CheckmarkIcon/>}
                                    text="Account erstellen"
                    />
                </HStack>
            </VStack>
        </div>
    );
}




import {InferType, number, object} from "yup";
import {useSignupContext} from "../../../util/SignupProvider";
import {createForm} from "@felte/solid";
import {validator} from "@felte/validator-yup";
import {GradientButton} from "../../../components/GradientButton";
import {TextButton} from "../../../components/TextButton";
import LoginIcon from "../../../components/LoginIcon";
import {API_HOST, DOWNLOAD_URL, SIGNIN_URL} from "../../../static";

export function Setup4OpenMiniPlan() {
    const [state, {incrementUiFrame, updatePhoneNumber, updateLoginRedisToken}] = useSignupContext();

    return (
        <div>
            <img src="/mini2.webp" alt="mini"/>
            <div>
                <h1 class="text-lg font-semibold">Willkommen bei MiniPlan!</h1>
                <h2 class="text-pink-500 text-xs font-semibold mb-3">Dein Account wurde erstellt! Du kannst dir die App
                    über den Knopf unten herunterladen.</h2>

                <div class="flex space-x-2">
                    <GradientButton onClick={() => window.location.href = DOWNLOAD_URL} text="App Herunterladen"/>
                    {/*<GradientButton type="submit" iconRight={true} icon={<LoginIcon/>} onClick={() => {*/}
                    {/*    window.location.href = `${SIGNIN_URL}/${state.loginRedisToken}`*/}
                    {/*}} text="In MiniPlan einloggen"/>*/}
                </div>
            </div>
        </div>
    );
}


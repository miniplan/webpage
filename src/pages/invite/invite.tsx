import {Resource, Show, Suspense} from "solid-js";
import {InviteData} from "./Invite.data";
import {Dynamic} from "solid-js/web";
import {SignupProvider} from "../../util/SignupProvider";
import {Setup1Explainer} from "./setup/Setup1Explainer";
import {Setup2Signup} from "./setup/Setup2Signup";
import {Setup3Otp} from "./setup/Setup3Otp";
import {Setup4OpenMiniPlan} from "./setup/Setup4OpenMiniPlan";
import {TextButton} from "../../components/TextButton";
import {homeRoute} from "../../routes";
import {useNavigate, useRouteData, useSearchParams} from "@solidjs/router";

export const JOB_INTERNAL_WAITING_FOR_INPUT = 0;


export default function Invite() {
    const [searchParams, setSearchParams] = useSearchParams();
    const inviteData = useRouteData<Resource<InviteData>>();

    const components = {
        [0]: Setup1Explainer,
        [1]: Setup2Signup,
        [2]: Setup3Otp,
        [3]: Setup4OpenMiniPlan,
    };

    if (!searchParams.page) {
        setSearchParams({page: 0})
    }

    return (
        <Suspense>
            <div class="min-h-screen flex flex-col justify-center overflow-hidden py-6 py-12">
                <div class="relative p-6 bg-white mx-auto max-w-sm rounded-xl shadow-lg">
                    <Show when={
                        // @ts-ignore
                        inviteData()?.exists} fallback={<InvalidToken/>}>
                        <SignupProvider uiFrame={JOB_INTERNAL_WAITING_FOR_INPUT} user={null} token={
                            // @ts-ignore
                            inviteData()?.token}
                                        phoneNumber={null} firstname={null} lastname={null} loginRedisToken={null}>
                            <Dynamic component={components[searchParams.page]}/>
                        </SignupProvider>
                    </Show>
                </div>
            </div>
        </Suspense>
    );
}

function InvalidToken() {
    const navigate = useNavigate();

    return (
        <div>
            <div>
                <h1 class="text-lg font-semibold">Ups, das hat nicht funktioniert :/</h1>
                <h2 class="text-pink-500 font-semibold text-xs mb-4">Dieser Einladungs-Link scheint entweder nicht zu
                    existieren, oder wurde schon genutzt.</h2>
                <TextButton onClick={() => navigate(homeRoute.path)}>Zurück zur Startseite</TextButton>
            </div>
        </div>
    );
}

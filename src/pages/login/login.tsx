import {InferType, number, object, string} from "yup";
import {createForm} from "@felte/solid";
import {validator} from "@felte/validator-yup";
import {FormControl, FormLabel, HStack, Input, InputGroup, InputLeftAddon, VStack} from "@hope-ui/solid";
import {createEffect, createSignal, Show, Suspense} from "solid-js";
import {isValidPhoneNumber} from "libphonenumber-js";
import {errors} from "../../util/errorCodes";
import {API_HOST} from "../../static";
import {ErrorIcon} from "../../components/ErrorIcon";
import {GreenSuccessButton} from "../../components/GreenSuccessButton";
import {GradientButton} from "../../components/GradientButton";
import CheckmarkIcon from "../../components/CheckmarkIcon";
import {SimpleTextButton} from "../../components/SimpleTextButton";
import {ArrowRightIcon} from "../../components/ArrowRightIcon";
import {useAppContext} from "../../util/AppProvider";
import {appRoute, privacyRoute} from "../../routes";
import {initApollo} from "../../util/ApolloHandler";
import {useNavigate} from "@solidjs/router";

const phoneNumberSchema = object({
    phoneNumber: string().required().test((number) => isValidPhoneNumber(number, "AT"))
});

const otpSchema = object({
    otp: string().required()
});

export default function Login() {
    const [state, {login, updateTokens, refreshRequired, setApollo, logout}] = useAppContext();

    const [counter, setCounter] = createSignal(60);
    const [phoneNumber, setPhoneNumber] = createSignal(null);
    const [phoneErrorText, setPhoneErrorText] = createSignal(null);
    const [loginRequested, setLoginRequested] = createSignal(false)

    const navigate = useNavigate();

    createEffect(() => {
        const sess = state.appSession;
        if (sess !== undefined && sess !== null) {
            login(sess);
            setApollo(initApollo({
                appSession: state.appSession,
                updateTokens, refreshRequired, logout
            }))

            //don't use navigate since that would not re-execute restClient setup in app.tsx
            navigate(appRoute.path);
        }
    });

    function setupCounter() {
        const interval = setInterval(() => {
            if (counter() > 0) {
                setCounter(counter() - 1)
            } else {
                clearInterval(interval);
            }
        }, 1100);
    }

    const {
        form: phoneForm,
        errors: phoneErrors,
        isValid: phoneIsValid
    } = createForm<InferType<typeof phoneNumberSchema>>({
        extend: validator({schema: phoneNumberSchema}),
        onSubmit: async (values) => {
            const validPhone = isValidPhoneNumber(values.phoneNumber, "AT")
            if (!validPhone) {
                setPhoneErrorText("Ungültige Handynummer!")
                return
            }

            const codeRequest = await fetch(`${API_HOST}/login`, {
                method: "POST",
                body: JSON.stringify({phoneNumber: values.phoneNumber})
            });

            if (codeRequest.status !== 200) {
                const error = await codeRequest.text();
                setPhoneErrorText(errors[error] ?? error);
                return
            }

            setLoginRequested(true);
            setPhoneNumber(values.phoneNumber);
        }
    })

    const {form: codeForm, errors: codeErrors, isValid: codeIsValid} = createForm<InferType<typeof otpSchema>>({
        extend: validator({schema: otpSchema}),
        onSubmit: async (values) => {
            const loginRequest = await fetch(`${API_HOST}/login/confirm`, {
                method: "POST",
                body: JSON.stringify({
                    phoneNumber: phoneNumber(),
                    otp: values.otp,
                })
            });

            if (loginRequest.status !== 200) {
                const error = await loginRequest.text();
                setPhoneErrorText(errors[error] ?? error);
                return
            }

            login(await loginRequest.json());

            navigate(appRoute.path);
        },
    });

    let phoneNumberRef = null;

    async function retryClicked() {
        // await sendCode(state.token, phoneNumberRef.value);
        setCounter(60);
        setupCounter();
    }

    function resetError() {
        setPhoneErrorText(null);
    }

    return (
        <Suspense>
            <div class="min-h-screen flex flex-col justify-center overflow-hidden py-6 py-12">
                <div class="relative p-6 bg-white mx-auto max-w-sm rounded-xl shadow-lg">
                    <div>
                        <h1 class="text-lg font-semibold">Willkommen bei MiniPlan!</h1>
                        <h2 class="text-pink-500 text-xs font-semibold mb-1">Gib deine Handynummer ein, damit wir dir
                            einen
                            Einmalcode schicken können</h2>

                        <h3 class="text-xs mb-3">Mit dem Einloggen stimmst du der <a class="underline" href={privacyRoute.path}>Datenschutzerklärung</a> zu</h3>

                        <Show when={phoneErrorText() !== null}>
                            <div class="flex justify-content items-center text-red-600 space-x-1 mb-2">
                                <ErrorIcon/>
                                <p class="text-red-600 text-xs font-semibold">{phoneErrorText()}</p>
                            </div>
                        </Show>

                        <VStack as="form" ref={phoneForm} spacing="$5" alignItems="stretch" maxW="$96" mx="auto">
                            <VStack>
                                <FormControl required invalid={!!phoneErrors("phoneNumber")}>
                                    <FormLabel>Handynummer</FormLabel>
                                    <InputGroup>
                                        <InputLeftAddon>+43</InputLeftAddon>
                                        <Input type="tel" name="phoneNumber" ref={phoneNumberRef}
                                               placeholder="123 456789"
                                               pattern="[0-9]*"
                                               oninput={(e) => {
                                                   // @ts-ignore
                                                   // e.target.value = format(e.target.value)
                                               }} onfocus={resetError}/>
                                    </InputGroup>
                                </FormControl>
                            </VStack>

                            <HStack justifyContent="flex-end">
                                <Show when={!loginRequested()}
                                      fallback={<GreenSuccessButton iconRight={true} icon={CheckmarkIcon}
                                                                    text="Code gesendet"
                                                                    onClick={() => {
                                                                    }}/>}>
                                    <GradientButton text="Code senden" disabled={!phoneIsValid()}
                                                    onClick={() => setupCounter()}/>
                                </Show>
                            </HStack>
                        </VStack>

                        <VStack as="form" ref={codeForm} spacing="$5" alignItems="stretch" maxW="$96" mx="auto">
                            <FormControl required invalid={!!codeErrors("otp")} disabled={!phoneIsValid()}
                                // @ts-ignore
                                         disabled:class="cursor-default">
                                <FormLabel>Einmalcode</FormLabel>
                                <Input type="text" name="otp" placeholder="123456"/>
                            </FormControl>

                            <HStack>
                                <SimpleTextButton onClick={() => {
                                    retryClicked()
                                }} disabled={counter() > 0}>Code erneut anforden<Show
                                    when={counter() > 0}><span> (in {counter()} Sekunden)</span></Show></SimpleTextButton>
                            </HStack>

                            <HStack spacing="$1" justifyContent="flex-end" className={"text-gray-400"}>
                                <GradientButton type="submit" disabled={!codeIsValid()} iconRight={true}
                                                icon={<ArrowRightIcon/>}
                                                text="Einloggen"
                                />
                            </HStack>
                        </VStack>
                    </div>
                </div>
            </div>
        </Suspense>
    );
}




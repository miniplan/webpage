import {Accessor, createEffect, createSignal, For, onMount, Setter, Show} from "solid-js";
import {createMutation, createQuery, gql} from "@merged/solid-apollo";
import {Checkbox, Input, InputGroup, InputRightElement} from "@hope-ui/solid";
import {GradientButton} from "../../components/GradientButton";
import {TextButton} from "../../components/TextButton";
import clickOutside from "../../util/clickOutside";
import CloseIcon from "../../components/CloseIcon";

export type MinistrantModalProps = {
    isOpen: Accessor<boolean>
    setOpen: Setter<boolean>
}

type GQLMinistrantWrapper = {
    Ministrant: GQLMinistrant[],
    User: Array<{
        ministrantRel: GQLMinistrant
    }>,
    UserMinistrant: Array<{
        ministrantRel: GQLMinistrant
    }>
}

type GQLMinistrant = {
    id: string,
    firstname: string,
    lastname: string
}

type SetupData = {
    user: GQLMinistrant,
    ministranten: GQLMinistrant[],
    selectedMinistranten: GQLMinistrant[]
}

export default function MinistrantModal(props: MinistrantModalProps) {
    const data = createQuery<GQLMinistrantWrapper>(gql`
        query GetMinistrantenSetupList {
            Ministrant(where: {
                _not: {usersRel: {}}
            }, order_by: [{lastname: asc}, {firstname: asc}]) {
                id
                firstname
                lastname
            }
            User {
                ministrantRel {
                    id
                    firstname
                    lastname
                }
            }
            UserMinistrant {
                ministrantRel {
                    id
                    firstname
                    lastname
                }
            }
        }
    `);
    const [mutate, mutationData] = createMutation(gql`
        mutation InsertDeleteUserMinistrant(
            $deleteMinistrant: [uuid!],
            $insertMinistrant: [UserMinistrant_insert_input!]!
        ) {
            insert_UserMinistrant(objects: $insertMinistrant) {
                affected_rows
            }
            delete_UserMinistrant(where: {ministrant: {_in: $deleteMinistrant}}) {
                affected_rows
            }
        }
    `);

    const [selectedMinistranten, setSelectedMinistranten] = createSignal<string[]>([]);

    const setupData = () => {
        return {
            user: data()?.User[0].ministrantRel,
            ministranten: data()?.Ministrant,
            selectedMinistranten: data()?.UserMinistrant.map(i => i.ministrantRel)
        } as SetupData;
    }

    createEffect(() => {
        setSelectedMinistranten(setupData().selectedMinistranten?.map(i => i.id));
    });

    let modal;

    onMount(() => {
        clickOutside(modal, () => {
            props.setOpen(false)
        });
    });

    function onCheckboxChange(ministrant: GQLMinistrant, checked: boolean) {
        if (checked) {
            setSelectedMinistranten([...selectedMinistranten(), ministrant.id]);
        } else {
            setSelectedMinistranten(selectedMinistranten().filter(item => item !== ministrant.id));
        }
    }

    const [search, setSearch] = createSignal("");


    return (
        <div class="modal" classList={{"modal-open": props.isOpen()}}>
            <div class="modal-box" ref={modal}>
                <h1 class="text-lg font-semibold pb-2">Aufgaben von</h1>
                <InputGroup>
                    <Input class="rounded-full" variant="filled" placeholder="Suchen" value={search()}
                           onInput={(e) => {
                               // @ts-ignore
                               setSearch(e.target.value)
                           }}/>
                    <InputRightElement>
                        <Show when={search().length > 0}>
                            <button class="flex place-content-center hover:bg-gray-100 rounded-full p-1" onClick={() => setSearch("")}>
                                <CloseIcon />
                            </button>
                        </Show>
                    </InputRightElement>
                </InputGroup>

                <div class="flex flex-col space-y-2 pt-2">
                    <Show when={setupData()?.user != null}>
                        <MinistrantRow ministrant={setupData()?.user} isSelf={true}
                                       selectedMinistranten={setupData()?.selectedMinistranten}
                                       onChange={(ministrant, checked) => onCheckboxChange(ministrant, checked)}/>
                    </Show>

                    <For each={setupData()?.ministranten}>{(data, i) =>
                        <Show
                            when={search().length === 0
                                || data.firstname.toLowerCase().includes(search().toLowerCase())
                                || data.lastname.toLowerCase().includes(search().toLowerCase())}>
                            <MinistrantRow ministrant={data} isSelf={false}
                                           selectedMinistranten={setupData()?.selectedMinistranten}
                                           onChange={(ministrant, checked) => onCheckboxChange(ministrant, checked)}/>
                        </Show>
                    }</For>
                </div>

                <div class="modal-action">
                    <TextButton onClick={() => props.setOpen(false)}>Abbrechen</TextButton>
                    <GradientButton text="Speichern" onClick={async () => {
                        let oldSelection = setupData()?.selectedMinistranten.map(i => i.id);
                        let newSelection = selectedMinistranten();

                        let all = new Set();
                        for (let item of newSelection) {
                            all.add(item);
                        }

                        for (let item of oldSelection) {
                            all.add(item);
                        }

                        let allArray = Array.from(all);

                        let added = [...allArray];
                        for (let i = 0; i < oldSelection.length; i++) {
                            added = added.filter(x => x !== oldSelection[i]);
                        }

                        let deleted = [...allArray];
                        for (let i = 0; i < newSelection.length; i++) {
                            deleted = deleted.filter(x => x !== newSelection[i]);
                        }

                        await mutate({
                            variables: {
                                deleteMinistrant: deleted,
                                insertMinistrant: added.map(i => {
                                    return {ministrant: i}
                                }) as Array<{ ministrant: string }>
                            }
                        });

                        props.setOpen(false);
                    }}/>
                </div>
            </div>
        </div>
    );
}

type MinistrantRowProps = {
    ministrant: GQLMinistrant,
    isSelf: boolean,
    selectedMinistranten: GQLMinistrant[],
    onChange: (ministrant: GQLMinistrant, checked: boolean) => void
}

function MinistrantRow(props: MinistrantRowProps) {
    function isChecked(ministrant: GQLMinistrant) {
        return props.selectedMinistranten.find(
            (selectedMinistrant) => selectedMinistrant.id == ministrant.id
        ) !== undefined;
    }

    const name = () => {
        let str = `${props.ministrant.firstname} ${props.ministrant.lastname}`;
        if (props.isSelf) {
            str += " (Du)";
        }

        return str;
    }
    return (
        <label class="flex cursor-pointer">
            <Checkbox defaultChecked={isChecked(props.ministrant)} bg="$primary1"
                      onChange={(e) => props.onChange(props.ministrant, e.currentTarget.checked)}/>
            <span class="label-text">{name}</span>
        </label>
    );
}

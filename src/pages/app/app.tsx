import {createEffect, createSignal, For, Show} from "solid-js";
import {createQuery, gql, useApollo} from "@merged/solid-apollo";
import {changeTimezone} from "../../util/util";
import TaskIcon from "../../components/taskicons/TaskIcon";
import {IconProps} from "../../components/IconProps";
import GroupIcon from "../../components/GroupIcon";
import MinistrantModal from "./MinistrantModal";
import {SubtleTextButton} from "../../components/SubtleTextButton";

type GQLMesseType = {
    id: number,
    name: string,
    comment: string
}

type GQLMesse = {
    id: number,
    duration: number,
    comment: string,
    messeTypeRel: GQLMesseType
}

type GQLTaskType = {
    id: number,
    short_name: string,
    name: string
}

type GQLTask = {
    id: number,
    description: string,
    duration: number,
    start: string,
    messeRel: GQLMesse,
    taskTypeRel: GQLTaskType
}

type GQLMinistrant = {
    id: string,
    firstname: string,
    lastname: string,
    tasksRel: GQLTask[]
}

type GQLUserMinistrantWrapper = {
    UserMinistrant: Array<{ ministrantRel: GQLMinistrant }>
}


type Task = {
    ministrant: GQLMinistrant,
    task: GQLTask,
    messeId: number
}

export default function App() {
    const client = useApollo();

    const [modalOpen, setModalOpen] = createSignal(false);

    const gqlTasks = createQuery<GQLUserMinistrantWrapper>(gql`
        query GetUserMinistrantsTasks($after: timestamp!) {
            UserMinistrant {
                ministrantRel {
                    id
                    firstname
                    lastname
                    tasksRel(where: {start: {_gte: $after}}) {
                        id
                        description
                        duration
                        start
                        messeRel {
                            id
                            duration
                            comment
                            messeTypeRel {
                                id
                                name
                                comment
                            }
                        }
                        taskTypeRel {
                            id
                            short_name
                            name
                        }
                    }
                }
            }
        }
    `, {variables: {after: new Date().toISOString()}})

    createEffect(async () => {
        if (!modalOpen()) {
            await client.refetchQueries({include: ["GetUserMinistrantsTasks"]});
        }
    });

    const selectedMinistranten = () => gqlTasks()?.UserMinistrant.length ?? 0;
    const tasks = () => gqlTasks()?.UserMinistrant.flatMap(userMinistrant => {
        const ministrant = userMinistrant.ministrantRel;
        return userMinistrant.ministrantRel.tasksRel.map(gqlTask => {
            const task: Task = {
                ministrant, task: gqlTask, messeId: gqlTask.messeRel.id
            }

            return task;
        });
    }).sort((a, b) => {
        // @ts-ignore
        return new Date(a.task.start) - new Date(b.task.start);
    });

    return (
        <div class="flex flex-col justify-center py-6">
            <div class="container relative p-6 bg-white mx-auto max-w-sm md:max-w-md rounded-xl shadow-lg ">
                <h1 class="flex-1 text-lg font-semibold">Nächste Einteilungen</h1>
                <div class="flex space-x-2">
                    <SubtleTextButton onClick={() => {
                        setModalOpen(!modalOpen())
                    }}>
                        <div class="flex items-center space-x-1">
                            <GroupIcon/>
                            <span>Ministranten editieren</span>
                        </div>
                    </SubtleTextButton>
                    {/*<SimpleTextButton>Kalender exportieren</SimpleTextButton>*/}
                </div>

                <MinistrantModal isOpen={modalOpen} setOpen={setModalOpen}/>

                <Show when={selectedMinistranten() == 0}>
                    <div class="flex flex-col items-center justify-center">
                        <img width="280rem" src="/altar_boy_bubble.webp" alt="ministrant"/>
                        <h2 class="font-semibold text-xs">Keine Ministranten ausgewählt!</h2>
                    </div>
                </Show>
                <div class="overflow-y-auto space-y-3 mt-3" style="max-height: 70vh">
                    <For each={tasks()}>{(task, i) =>
                        <Row task={task} multipleMinistranten={selectedMinistranten() > 1}></Row>
                    }</For>
                </div>
            </div>
        </div>
    );
}

type RowProps = { task: Task, multipleMinistranten: boolean }

function Row(props: RowProps) {
    const {date, hour, minute} = changeTimezone(props.task.task.start);
    const iconProps: IconProps = {
        width: "w-12",
        height: "h-12"
    }

    const title = () => {
        let string = "";
        if (props.multipleMinistranten) {
            string += `${props.task.ministrant.firstname}: `
        }
        string += `${hour}:${minute}`

        return string;
    }

    return (<div class="flex space-x-3 place-items-center hover:bg-gray-100 rounded-xl cursor-pointer">
        <div>
            <TaskIcon type={props.task.task.taskTypeRel.short_name} iconProps={iconProps}/>
        </div>
        <div>
            <h1 class="text-md font-semibold">{title()}</h1>
            <h2>{date.toDateString()}</h2>
            <h3 class="text-pink-400">{props.task.task.taskTypeRel.name} - {props.task.task.messeRel.messeTypeRel.name}</h3>
        </div>
    </div>);
}



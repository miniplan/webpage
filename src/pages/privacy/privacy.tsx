import {TextButton} from "../../components/TextButton";
import {useNavigate} from "solid-app-router";
import {homeRoute} from "../../routes";

export default function Privacy() {
    const navigate = useNavigate();


    return (<div
        class="min-h-screen flex flex-col justify-center overflow-hidden py-6 py-12">
        <div class="relative py-8 bg-white mx-auto max-w-sm md:max-w-3xl rounded-lg shadow-lg">
            <div class="mb-3 px-6">
                <h1 class="text-lg font-semibold">MiniPlan Datenschutzerklärung</h1>

                <p>Zuletzt aktualisiert: 2022-11-20</p>

                <h2 class="text-md font-semibold">Datenerfassung</h2>

                <div class="ml-1 space-y-3">
                    <h3 class="text-md font-semibold">Generelles</h3>

                    <div>
                        Wenn du die App benutzt, oder die Webseite aufrufst, werden automatisch unter anderem folgende
                        Daten zum Server gesendet
                        <ul class="list-decimal list-inside">
                            <li>deine IP Adresse</li>
                            <li>den Anfragetyp (z.B. Einteilungen anzeigen)</li>
                            <li>das Datum und die Zeit der Anfrage</li>
                            <li>die Appversion</li>
                        </ul>

                        Diese Daten werden 7 Tage gespeichert.
                    </div>

                    <h3 class="text-md font-semibold">App</h3>

                    <div>
                        <p>Wenn du dich für unsere App anmeldest, fragen wir nach</p>

                        <ul class="list-disc list-inside">
                            <li>deinem vollen Namen und</li>
                            <li>deiner Handynummer</li>
                        </ul>
                    </div>

                    <div>
                        <p>Diese Daten werden nur verwendet um</p>

                        <ul class="list-disc list-inside">
                            <li>den Login bereitzustellen (Handynummer) und</li>
                            <li>deinen Nutzer-Account einer Person zuzuordnen (Name).</li>
                        </ul>
                    </div>

                    <p>Bitte beachte, dass wir für den Login mit deiner Handynummer den externen Dienstleister Twilio
                        nutzen.
                        Twilios Datenschutzerklärung findest du <a href="https://www.twilio.com/legal/privacy"
                                                                   class="underline">hier</a>.</p>

                    <div>
                        <h4>Weitere Daten die wir speichern</h4>

                        <ul class="list-disc list-inside">
                            <li>Deine Aufgaben als Ministrant</li>
                            <li>Crash-Reports, sollte die App einmal abstürzen (enthalten Daten über dein Handy)</li>
                        </ul>
                    </div>


                    <h3 class="font-semibold">Webseite</h3>

                    <div>
                        <p>Auf der Webseite werden nur minimale Analytics erfasst. Wir speichern keine eindeutigen
                            Identifikationsmerkmale deines
                            Browsers.</p>

                    </div>

                    <p>Deine Daten werden weder verkauft, noch in irgendeiner Art und Weise weitergegeben.</p>
                </div>
            </div>

            <div class="px-3">
                <TextButton onClick={
                    () => window.history.length === 1 ? navigate(homeRoute.path) : navigate(-1)
                }>Zurück</TextButton>
            </div>
        </div>
    </div>);
}

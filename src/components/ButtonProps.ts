import {JSX} from "solid-js";

export interface ButtonProps {
    text?: string,
    disabled?: boolean,
    onClick?: () => void,
    icon?: JSX.Element,
    iconRight?: boolean,
    type?: "submit" | "reset" | "button",
}

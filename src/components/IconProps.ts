
export type IconProps = {
    width?: string,
    height?: string,
    margin?: string,
    className?: string,
}

export const DefaultIconProps: IconProps = {
    width: "w-5", height: "h-5"
}

export const mergeIconProps = (props?: IconProps, defaultMargin: string = "") => {
    return [
        props?.height || DefaultIconProps.height,
        props?.width || DefaultIconProps.width,
        props?.margin || defaultMargin,
        props?.className || ""
    ]
}

export const interpolateClassName = (props: IconProps, defaultMargin: string) => {
    return mergeIconProps(props, defaultMargin).join(" ")
}

import {ButtonProps} from "./ButtonProps";
import {Show} from "solid-js";

export function GreenSuccessButton(props: ButtonProps) {
    return (
        <button disabled={props.disabled} onClick={props.onClick}
                class={`flex bg-green-500 hover:green-500 text-white py-2 px-4 rounded-full font-semibold ` +
                    "text-xs items-center cursor-default disabled:text-gray-600 disabled:bg-gray-300 disabled:from-gray disabled:to-gray h-9"}>

            <Show when={props.iconRight} fallback={props.icon}>
                {props.text}
            </Show>

            <Show when={props.iconRight} fallback={<span>{props.text}</span>}>
                {props.icon}
            </Show>
        </button>
    );
}

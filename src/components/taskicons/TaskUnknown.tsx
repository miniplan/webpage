import {IconProps, interpolateClassName} from "../IconProps";

export function TaskUnknown(props?: IconProps) {
    return (
        <svg class={"fill-current " + interpolateClassName(props, null)} id="vector" xmlns="http://www.w3.org/2000/svg"
             width="100" height="100" viewBox="0 0 100 100">
            <path fill="#FF000000"
                  d="m256,336a24,24 0,1 0,24 24,24.027 24.027,0 0,0 -24,-24zM256,368a8,8 0,1 1,8 -8,8.009 8.009,0 0,1 -8,8z"
                  id="path_0"/>
            <path fill="#FF000000"
                  d="m448,120a8,8 0,0 0,-2.343 -5.657l-96,-96a8,8 0,0 0,-5.657 -2.343h-240a40.045,40.045 0,0 0,-40 40v400a40.045,40.045 0,0 0,40 40h304a40.045,40.045 0,0 0,40 -40zM352,43.313 L420.687,112h-44.687a24.027,24.027 0,0 1,-24 -24zM432,456a24.028,24.028 0,0 1,-24 24h-304a24.028,24.028 0,0 1,-24 -24v-400a24.028,24.028 0,0 1,24 -24h232v56a40.045,40.045 0,0 0,40 40h56z"
                  id="path_1"/>
            <path fill="#FF000000"
                  d="m256,160a64.072,64.072 0,0 0,-64 64,8 8,0 0,0 16,0 48,48 0,1 1,48 48,8 8,0 0,0 -8,8v32a8,8 0,0 0,16 0v-24.5a64,64 0,0 0,-8 -127.5z"
                  id="path_2"/>
        </svg>
    );
}

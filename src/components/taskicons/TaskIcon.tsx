import {TaskKr} from "./TaskKr";
import {TaskAl} from "./TaskAl";
import {TaskTh} from "./TaskTh";
import {TaskKa} from "./TaskKa";
import {TaskUnknown} from "./TaskUnknown";
import {TaskAk} from "./TaskAk";
import {TaskMb} from "./TaskMb";
import {TaskMi} from "./TaskMi";
import {IconProps} from "../IconProps";

export type TaskIconProps = {
    type: string,
    iconProps: IconProps
}

export default function TaskIcon(props: TaskIconProps) {
    switch (props.type) {
        case "KR":
            return <TaskKr {...props.iconProps}/>
        case "AL":
            return <TaskAl {...props.iconProps}/>
        case "TH":
            return <TaskTh {...props.iconProps}/>
        case "KA":
            return <TaskKa {...props.iconProps}/>
        case "WW":
            return <TaskUnknown {...props.iconProps}/>
        case "AK":
            return <TaskAk {...props.iconProps}/>
        case "MB":
            return <TaskMb {...props.iconProps}/>
        case "MI":
            return <TaskMi {...props.iconProps}/>
        case "ZE":
            return <TaskUnknown {...props.iconProps}/>
        case "KW":
            return <TaskUnknown {...props.iconProps}/>
    }
}

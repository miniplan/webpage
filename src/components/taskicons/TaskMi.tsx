import {IconProps, interpolateClassName} from "../IconProps";

export function TaskMi(props?: IconProps) {
    return (
        <svg class={"fill-current " + interpolateClassName(props, null)} id="vector" xmlns="http://www.w3.org/2000/svg"
             width="100" height="100" viewBox="0 0 100 100">
            <path fill="#FF000000"
                  d="m245.491,245.396 l-40,40c-5.858,5.857 -5.858,15.355 0,21.213 5.858,5.859 15.355,5.858 21.213,0l40,-40c5.858,-5.857 5.858,-15.355 0,-21.213 -5.857,-5.857 -15.355,-5.857 -21.213,0z"
                  id="path_0"/>
            <path fill="#FF000000"
                  d="m474,217.703c50.859,-50.962 50.768,-128.635 -0.1,-179.503 -51.157,-51.157 -129.275,-50.7 -179.609,-0.004 -29.366,29.572 -41.201,69.381 -35.908,107.442l-242.071,256.55c-22.264,23.764 -21.666,60.415 1.359,83.44l8.801,8.801c23.026,23.028 59.68,23.62 83.479,1.323l256.511,-242.035c40.964,5.695 81.324,-9.039 107.538,-36.014zM452.688,59.413c35.478,35.479 38.764,87.317 9.861,125.828l-135.689,-135.69c38.539,-28.923 90.37,-25.595 125.828,9.862zM89.402,473.896c-11.881,11.129 -30.205,10.831 -41.718,-0.68l-8.801,-8.801c-11.512,-11.512 -11.811,-29.836 -0.716,-41.679l229.736,-243.476c12.713,28.908 35.975,52.2 64.935,64.936zM315.508,196.592c-34.212,-34.213 -37.47,-87.834 -9.783,-125.75l135.533,135.533c-37.916,27.687 -91.536,24.43 -125.75,-9.783z"
                  id="path_1"/>
        </svg>
    );
}

import {IconProps, interpolateClassName} from "../IconProps";

export function TaskTh(props?: IconProps) {
    return (
        <svg class={"fill-current " + interpolateClassName(props, null)} id="vector" xmlns="http://www.w3.org/2000/svg"
             width="100" height="100" viewBox="0 0 100 100">
            <path fill="?attr/colorOnSurface"
                  d="M46.5,5.4c-3.1,1.4 -6.1,4.3 -5.3,5.2 0.3,0.2 1.7,-0 3.2,-0.6 5.7,-2.1 13.5,-0.3 18.3,4.2l2.3,2.1 0,-2.2c0,-7.5 -10.2,-12.3 -18.5,-8.7z"
                  stroke="#00000000" id="path_0"/>
            <path fill="?attr/colorOnSurface"
                  d="M16.6,11.5c-5,4.7 -2.4,13 5.1,16.1 4.2,1.8 8.8,0.7 18.8,-4.4 5.9,-3.1 8,-3.6 13.7,-3.7 6.5,-0 6.6,-0.1 5,-1.9 -2.4,-2.7 -7.9,-4.8 -10.5,-4.1 -1.2,0.4 -5.7,2 -9.9,3.6 -9,3.4 -14.7,3.7 -17.1,1.1 -2,-2.2 -2.2,-6.3 -0.5,-8 2.4,-2.4 -1.9,-1.3 -4.6,1.3z"
                  stroke="#00000000" id="path_1"/>
            <path fill="?attr/colorOnSurface"
                  d="M7.7,19.6c-1.3,1.4 -0.7,6.4 1.3,10.3 1.3,2.7 3.3,4.6 6.3,6 5.3,2.6 9,2.6 14.2,0.2l4,-1.9 -3.6,-0.1c-9.5,-0.2 -19,-5.9 -20.7,-12.2 -0.4,-1.7 -1.1,-2.7 -1.5,-2.3z"
                  stroke="#00000000" id="path_2"/>
            <path fill="?attr/colorOnSurface"
                  d="M56.1,26.8c2.4,1.6 5.8,4.8 7.4,7.2 2.6,3.7 3,5.3 3,10.7 0,3.4 0.3,6.3 0.6,6.3 0.4,-0 2.1,-1.3 3.8,-2.9 2.6,-2.4 3.1,-3.7 3.1,-7.4 0,-2.9 -0.8,-5.5 -2.3,-7.8 -2.9,-4.4 -11,-8.8 -16.1,-8.9l-4,-0 4.5,2.8z"
                  stroke="#00000000" id="path_3"/>
            <path fill="?attr/colorOnSurface"
                  d="M44.9,29.8c-1.5,0.9 -5.5,4.1 -8.8,6.9 -5.7,4.9 -7.2,5.6 -12.6,6.6 -1.7,0.2 -1.5,0.6 1.3,2.1 4.2,2.2 6.2,1.6 13.5,-4.1 7.1,-5.4 10.2,-6.7 12.7,-5.3 2.1,1.1 3.5,4.4 2.5,6 -1.3,2 1.4,0.9 2.9,-1.2 3.1,-4.3 0.9,-9.8 -4.6,-11.7 -3.9,-1.4 -3.8,-1.4 -6.9,0.7z"
                  stroke="#00000000" id="path_4"/>
            <path fill="?attr/colorOnSurface"
                  d="M59.8,43.9c-1.8,3.2 -7.5,5.5 -11.5,4.5 -2.9,-0.8 -3,1 -0.1,3 5.8,4.1 13.8,0.1 13.8,-6.9 0,-1.9 -0.1,-3.5 -0.2,-3.5 -0.2,-0 -1,1.3 -2,2.9z"
                  stroke="#00000000" id="path_5"/>
            <path fill="?attr/colorOnSurface"
                  d="M75.3,49.4c-2.5,3.9 -9.6,7.7 -15.9,8.4 -3.8,0.4 -10.1,-0.6 -14.7,-2.4 -2.9,-1.1 -0.7,3.4 3.4,7.1 3.5,3.2 4.5,3.5 9.8,3.5 3.3,-0 7.1,-0.4 8.6,-1l2.5,-1 -1.6,3.3c-0.9,1.7 -3.2,4.7 -5.1,6.4 -4,3.8 -3.7,4 1.5,1.4 7.9,-4.1 13.1,-12.6 14,-22.6 0.2,-3.6 0.3,-6.5 0.1,-6.5 -0.2,-0 -1.4,1.5 -2.6,3.4z"
                  stroke="#00000000" id="path_6"/>
            <path fill="?attr/colorOnSurface"
                  d="M34,57.1c-4.2,9.9 -2.6,20.7 4.3,28.6 11.1,12.5 29.3,12.5 40.5,-0.2 2.3,-2.6 4.7,-6.6 5.3,-8.9 1.5,-5.6 1.4,-13.5 -0.4,-17.7l-1.4,-3.4 -0.8,3.5c-1.7,8 -8.4,17.3 -14.5,20.5 -9.7,5 -20.6,1.5 -28,-9 -1.3,-1.8 -1.2,-1.8 1,0.2 4.9,4.3 11.4,5.4 16,2.8l2.5,-1.4 -2.8,-0.1c-7.1,-0.1 -13.4,-5.4 -17.1,-14.4l-2.3,-5.7 -2.3,5.2z"
                  stroke="#00000000" id="path_7"/>
        </svg>
    );
}

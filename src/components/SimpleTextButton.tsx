import {ButtonLinkProps} from "../util/ButtonLinkProps";

export function SimpleTextButton(props: ButtonLinkProps) {
    return (
        <button class="text-blue-700 disabled:text-gray-700 font-semibold text-xs disabled:cursor-default" disabled={props.disabled}
                onclick={props.onClick}>{props.children}</button>
    );
}

import {JSX, Show} from "solid-js";
import {ButtonLinkProps} from "../util/ButtonLinkProps";
import {ButtonProps} from "./ButtonProps";

export function GradientButton(props: ButtonProps) {
    const gradient = "bg-gradient-to-r from-indigo-500 to-pink-500";

    return (
        <button disabled={props.disabled} onClick={props.onClick}
                class={`flex ${!props.disabled ? gradient : ""} hover:from-pink-500 text-white py-2 px-4 hover:bg-pink-500 rounded-full font-semibold ` +
                    "text-xs items-center disabled:cursor-default disabled:text-gray-600 disabled:bg-gray-300 disabled:from-gray disabled:to-gray h-9"}>

            <Show when={props.iconRight} fallback={props.icon}>
                {props.text}
            </Show>

            <Show when={props.iconRight} fallback={<span>{props.text}</span>}>
                {props.icon}
            </Show>
        </button>
    );
}

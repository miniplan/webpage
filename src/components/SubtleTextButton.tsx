import {DOWNLOAD_URL} from "../static";
import {ButtonLinkProps} from "../util/ButtonLinkProps";

export function SubtleTextButton(props: ButtonLinkProps) {
    return (
        <button class="text-blue-700 disabled:text-gray-700 py-2 px-4 hover:bg-blue-200 bg-blue-100 rounded-full font-semibold text-xs" disabled={props.disabled}
                onclick={props.onClick}>{props.children}</button>
    );
}

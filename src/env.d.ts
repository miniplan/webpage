interface ImportMetaEnv {
    VITE_API_HOST: string;
    VITE_ANALYTICS?: string;
    VITE_MINIPLAN_DOWNLOAD_URL?: string;
    VITE_DEEPLINK?: string;
    VITE_ANALYTICS_HOST?: string;
    VITE_ANALYTICS_SCRIPT?: string;
    VITE_ANALYTICS_EVENT_API?: string;
}


import Home from "./pages/home/home";
import Invite from "./pages/invite/invite";
import InviteData from "./pages/invite/Invite.data";
import Privacy from "./pages/privacy/privacy";
import {Component} from "solid-js";
import Login from "./pages/login/login";
import App from "./pages/app/app";
import Plan from "./pages/plan/plan";
import {RouteDataFunc, RouteDefinition} from "@solidjs/router";
// import Login from "./pages/app/login/login";

export type RouteGroup = {
    [key: string]: RouteGroupDef
}
type RouteQuery = {
    name: string,
    buildQuery: (any: any) => string
}

export type RouteGroupDef = {
    path: string,
    buildPath?: (any) => string,
    component: Component,
    data?: RouteDataFunc,
    queries?: { [key: string]: RouteQuery }
    auth?: boolean
}

export const homeRoute: RouteGroupDef = {
    path: "/",
    component: Home
}

export const inviteRoute: RouteGroupDef = {
    path: "/invite/:token",
    component: Invite,
    data: InviteData
}

export const privacyRoute: RouteGroupDef = {
    path: "/privacy",
    component: Privacy
}

export const loginRoute: RouteGroupDef = {
    path: "/login",
    component: Login,
}

export const appRoute: RouteGroupDef = {
    path: "/app",
    component: App,
}

export const planRoute : RouteGroupDef = {
    path: "/plan",
    component: Plan,
}

export const routes: RouteDefinition[] = [
    appRoute,
    homeRoute,
    loginRoute,
    inviteRoute,
    privacyRoute
];

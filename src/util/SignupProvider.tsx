import {Component, createContext, JSX, useContext} from "solid-js";
import {createStore} from "solid-js/store";
import {Property} from "@stitches/core/types/css";
import {decrementCookie, incrementCookie, setCookie} from "./cookie";

export type User = {
    firstname: string;
    lastname: string;
}

export type SignupStore = [
    { uiFrame: number, firstname: string, lastname: string, user: User, token?: string, phoneNumber?: string, loginRedisToken?: string },
    {
        updateFirstname?: (newFirstname: string) => void;
        updateLastname?: (newLastname: string) => void;
        updateUser?: (newUser: User) => void;
        updateToken?: (token: string) => void;
        updatePhoneNumber?: (phoneNumber: string) => void;
        updateLoginRedisToken?: (redisToken: string) => void;
        incrementUiFrame?: () => void;
        decrementUiFrame?: () => void;
    }
];

const SignupContext = createContext<SignupStore>([
    {uiFrame: 0, firstname: null, lastname: null, user: null, token: null, phoneNumber: null, loginRedisToken: null}, {}
]);

export const SignupProvider: Component<{
    uiFrame: number, firstname: string, lastname: string, user: User,
    token: string, phoneNumber: string, loginRedisToken: string, children: JSX.Element
}> = (props) => {
    const [state, setState] = createStore({
        uiFrame: props.uiFrame || 0,
        firstname: props.firstname || null,
        lastname: props.lastname || null,
        user: props.user || null,
        token: props.token || "",
        phoneNumber: props.phoneNumber || "",
        loginRedisToken: props.loginRedisToken || "",
    });

    const store: SignupStore = [state, {
        updateFirstname(newFirstname: string) {
            setState("firstname", () => newFirstname);
        },
        updateLastname(newLastname: string) {
            setState("lastname", () => newLastname);
        },
        updateUser(newUser: User) {
            setState("user", () => newUser);
        },
        updateToken(newToken: string) {
            setState("token", () => newToken);
        },
        updatePhoneNumber(phoneNumber: string) {
            setState("phoneNumber", () => phoneNumber)
        },
        updateLoginRedisToken(loginRedisToken: string) {
            setState("loginRedisToken", () => loginRedisToken)
        },
        incrementUiFrame() {
            setState("uiFrame", (uiFrame) => uiFrame + 1)
        },
        decrementUiFrame() {
            setState("uiFrame", (uiFrame) => uiFrame - 1)
        }
    }];

    return (
        <SignupContext.Provider value={store}>
            {props.children}
        </SignupContext.Provider>
    );
}

export function useSignupContext() {
    return useContext(SignupContext);
}

import {setContext} from "@apollo/client/link/context";
import {ErrorResponse, onError} from "@apollo/client/link/error";
import {ApolloClientOptions, ApolloLink, createHttpLink, fromPromise, split} from "@apollo/client/core";
import {ApolloClient, InMemoryCache} from "@merged/solid-apollo";
import {AppSession} from "./AppProvider";
import {GraphQLWsLink} from "@apollo/client/link/subscriptions";
import {WebSocketLink} from "@apollo/client/link/ws";
import {getMainDefinition} from "@apollo/client/utilities";
import {API_HOST, DATA_HOST} from "../static";

export type ConnectionFn = {
    appSession: AppSession,
    updateTokens: (accessToken: string, refreshToken: string) => void,
    refreshRequired: () => boolean,
    logout: () => void
}

const refreshTokens = (connection: ConnectionFn) => fetch(`${API_HOST}/refresh`, {
    headers: {
        authorization: `Bearer ${connection.appSession.refreshToken}`
    }
}).then(r => r.json()).then(json => connection.updateTokens(json["newAccessToken"], json["newRefreshToken"])).catch((err) => {
    console.log(err);
    connection.logout();
});

export function initApollo(connection: ConnectionFn): ApolloClient<any> {
    const apolloContext = setContext((request, prevCtx) => ({
        headers: {
            Authorization: `Bearer ${connection.appSession.accessToken}`
        }
    }));

    const errorHandler = onError((errorResponse: ErrorResponse) => {
        if (errorResponse.graphQLErrors && errorResponse.graphQLErrors.find((error) => error.extensions.code === "invalid-jwt")) {
            return fromPromise(refreshTokens(connection)).flatMap(() => errorResponse.forward(errorResponse.operation));
        }
    });

    console.log(DATA_HOST);

    return new ApolloClient({
        link: ApolloLink.from([errorHandler, apolloContext, createHttpLink({uri: DATA_HOST})]),
        cache: new InMemoryCache()
    })
}


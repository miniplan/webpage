import {Component, createContext, JSX, useContext} from "solid-js";
import {createStore} from "solid-js/store";
import {Property} from "@stitches/core/types/css";
import {decrementCookie, incrementCookie, setCookie} from "./cookie";
import jwtDecode from "jwt-decode";
import {ApolloClient} from "@merged/solid-apollo";


export type AppStore = [
    {
        appSession: AppSession,
        accessExpiry: number,
        apolloClient: ApolloClient<any>
    },
    {
        login?: (appSession: AppSession) => void;
        logout?: () => void;
        updateTokens?: (accessToken: string, refreshToken: string) => void;
        refreshRequired?: () => boolean;
        setApollo?: (apolloClient: ApolloClient<any>) => void;
    }
];

export type AppSession = {
    accessToken: string,
    refreshToken: string,
    user: User
}

export type User = {
    userId: string;
    phoneNumber: string;
    firstname: string;
    lastname: string;
}

const AppContext = createContext<AppStore>([
    {appSession: null, accessExpiry: null, apolloClient: null}, {}
]);

export const AppProvider: Component<{ children: JSX.Element }> = (props) => {
    const lsStoredSession = JSON.parse(localStorage.getItem("session")) as AppSession;
    const [state, setState] = createStore({
        appSession: lsStoredSession || null,
        accessExpiry: lsStoredSession ? jwtDecode(lsStoredSession.accessToken)["exp"] : null,
        apolloClient: null,
    });

    const store: AppStore = [state, {
        login(appSession: AppSession) {
            if (appSession !== undefined) {
                localStorage.setItem("session", JSON.stringify(appSession));
                console.log(`Saving to localStorage: ${JSON.stringify(appSession)}`)
                setState("appSession", () => appSession);
                setState("accessExpiry", () => jwtDecode(appSession.accessToken)["exp"]);
            }
        },
        logout() {
            localStorage.removeItem("session");
            setState("appSession", () => null);
        },
        updateTokens(accessToken: string, refreshToken: string) {
            const decoded = jwtDecode(accessToken);

            setState("appSession", (appSession: AppSession) => {
                // console.log(`AppSession: ${JSON.stringify(app(setState())}`)
                // if (appSession === undefined) {
                //     appSession = JSON.parse(localStorage.getItem("session"));
                // }
                appSession.accessToken = accessToken;
                appSession.refreshToken = refreshToken;

                return appSession;
            });

            setState("accessExpiry", () => decoded["exp"]);

            if (state.appSession !== undefined) {
                localStorage.setItem("session", JSON.stringify(state.appSession));
                console.log(`Saving to localStorage: ${JSON.stringify(state.appSession)}`)
            }
        },
        refreshRequired() {
            return state.accessExpiry < new Date();
        },
        setApollo(apolloClient: ApolloClient<any>) {
            setState("apolloClient", () => apolloClient);
        }
    }]

    return (
        <AppContext.Provider value={store}>
            {props.children}
        </AppContext.Provider>
    );
};

export function useAppContext() {
    return useContext(AppContext);
}


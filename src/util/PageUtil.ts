import {Params} from "solid-app-router";

export function next(searchParams: Params) {
    return {page: parseInt(searchParams.page) + 1}
}

export function previous(searchParams: Params) {
    return {page: parseInt(searchParams.page) - 1}
}

type PageSearchParams = {
    page: string
}

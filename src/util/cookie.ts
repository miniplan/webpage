import Cookie from "js-cookie";
import {bool} from "yup";

export const setCookie = (token: string, id: string) => {
    return Cookie.set(token, id, { sameSite: "strict" });
}

export const getCookie = (token: string): string => {
    return Cookie.get(token);
}

export const hasCookie = (token: string): boolean => {
    return Cookie.get(token) !== undefined;
};

export const incrementCookie = (token: string) => {
    const cookie = getCookie(token)
    setCookie(token, String(parseInt(cookie) + 1))
}

export const decrementCookie = (token: string) => {
    const cookie = getCookie(token)
    setCookie(token, String(parseInt(cookie) - 1))
}


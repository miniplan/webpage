import {JSX} from "solid-js";

export type ButtonLinkProps = {
    onClick?: () => void;
    className?: string;
    children?: JSX.Element,
    disabled?: boolean
}

export const signupSuccess = "user_created"
export const errors = {
    "no_such_user": "Es existiert kein Nutzer mit dieser Handynummer!",
    "invalid_otp": "Falscher Einmalcode!",
    "wait_before_requesting_new_code": "Warte eine Minute, bevor du einen neuen Code beantragst!",
    "code_could_not_be_sent": "Code konnte nicht gesendet werden! Bitte versuche es später erneut!",
    "code_invalid": "Code ist ungültig",
    "phone_number_already_registered": "Handynummer ist bereits registriert",
    "phone_number_parse_failed": "Handynummer konnte nicht gelesen werden!",
    "invalid_token": "Falscher Token!",
    "token_has_already_been_used": "Dieser Token wurde schon benutzt!",
    "user_already_exists": "Dieser Nutzer existiert bereits!",
}

export const codeSuccess = "Code has been sent!"


export function padLeft(str: string, padWith: string, digits: number) {
    if (str.length < digits) {
        for (let i = 0; i < digits - str.length; i++) {
            str = padWith + str
        }
    }

    return str;
}

const DATETIME_REGEX = new RegExp("(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2})");

export function changeTimezone(dateStr: string): { date: Date, hour: string, minute: string } {
    const [_, year, month, day, hour, minute] = dateStr.match(DATETIME_REGEX);
    const date = new Date(Number(year), Number(month) - 1, Number(day));

    return {
        date: date,
        hour: hour,
        minute: minute
    }
}

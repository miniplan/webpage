export default function Card(props) {
    return (<div class="min-h-screen flex flex-col justify-center  py-6">
        <div class="relative p-5 bg-white mx-auto max-w-sm rounded-lg shadow-lg">
            {props.children}
        </div>
    </div>);
}

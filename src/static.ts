export const TLS_ENABLED: boolean = import.meta.env.VITE_TLS_ENABLED === "true";
export const API_HOST: string = (TLS_ENABLED ? "https://" : "http://") + import.meta.env.VITE_API_HOST;
export const DATA_HOST: string = (TLS_ENABLED ? "https://" : "http://") + import.meta.env.VITE_DATA_HOST;
export const SIGNIN_URL: string =(TLS_ENABLED ? "https://" : "http://") + import.meta.env.VITE_SIGNIN_URL;
export const DEEPLINK: string = import.meta.env.VITE_DEEPLINK;
export const DOWNLOAD_URL: string = import.meta.env.VITE_MINIPLAN_DOWNLOAD_URL;
export const ANALYTICS_ENABLED = import.meta.env.VITE_ANALYTICS_HOST !== undefined;

export const ANALYTICS_SCRIPT = import.meta.env.VITE_ANALYTICS_HOST + "/" + import.meta.env.VITE_ANALYTICS_SCRIPT;
export const ANALYTICS_EVENT_API = import.meta.env.VITE_ANALYTICS_HOST + "/" + import.meta.env.VITE_ANALYTICS_EVENT_API;

export default function NotFound() {
  return (
    <section class="text-gray-700 p-8">
      <h1 class="text-2xl font-bold">404 not found</h1>
      <p class="mt-4">whatever you were looking for is gone now</p>
    </section>
  );
}

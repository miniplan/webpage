// import "windi.css";

import {render} from "solid-js/web";
import App from "./app";
import "./index.css"
import {HopeProvider, HopeThemeConfig} from "@hope-ui/solid";
import {AppProvider} from "./util/AppProvider";
import { Router } from "@solidjs/router";

const config: HopeThemeConfig = {
    initialColorMode: "light",
    lightTheme: {
        colors: {
            primary5: "#dbeafe",
            primary9: "#1d4ed8",
        }
    },
}

render(() => (
        <HopeProvider config={config}>
            <AppProvider>
                <Router>
                    <App/>
                </Router>
            </AppProvider>
        </HopeProvider>
    ),
    document.getElementById("root")
);

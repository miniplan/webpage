import { appRoute, loginRoute, routes } from "./routes";
import { onMount, Show } from "solid-js";
import { useAppContext } from "./util/AppProvider";
import { ApolloProvider } from "@merged/solid-apollo";
import { initApollo } from "./util/ApolloHandler";
import { A, useMatch, useNavigate, useRoutes } from "@solidjs/router";
import { ANALYTICS_ENABLED, ANALYTICS_EVENT_API, ANALYTICS_SCRIPT, API_HOST } from "./static";
import { initAnalytics } from "./external/analytics";


export default () => {
    const [appState, { login, logout, updateTokens, refreshRequired, setApollo }] = useAppContext();

    const Route = useRoutes(routes);
    const navigate = useNavigate();
    const isAuth = useMatch(() => loginRoute.path);
    const isApp = useMatch(() => appRoute.path);


    onMount(async () => {
        try {
            await initAnalytics({
                enabled: ANALYTICS_ENABLED,
                webHost: API_HOST,
                script: ANALYTICS_SCRIPT,
                eventApi: ANALYTICS_EVENT_API
            });
        } catch (e) {
            console.error(e);
        }
    });

    if (appState.appSession === null && !isAuth() && isApp()) {
        window.location.replace(loginRoute.path);
        return <h1>Weiterleitung zum Login..</h1>;
    }

    if (appState.appSession !== null && appState.apolloClient === null) {
        const apollo = initApollo({
            appSession: appState.appSession, updateTokens, refreshRequired, logout
        });

        setApollo(apollo);
    }

    return (
        <div class="relative min-h-screen bg-gradient-to-r from-indigo-100 to-pink-100">
            <nav class="navbar">
                <Show when={appState.appSession !== null}>
                    <div class="flex-none">
                        <A href={appRoute.path}>
                            <div class="btn btn-ghost normal-case text-xl">MiniPlan</div>
                        </A>
                    </div>
                    {/*<div class="flex-1">*/}
                    {/*    <ul class="menu menu-horizontal px-1">*/}
                    {/*        <li><A href={planRoute.path}>Plan</A></li>*/}
                    {/*    </ul>*/}
                    {/*</div>*/}
                    {/*<div class="flex-none gap-2">*/}
                    {/*    <div class="flex justify-center items-center dropdown dropdown-end rounded-xl">*/}
                    {/*        /!*<div>*!/*/}
                    {/*        /!*    {appState.appSession?.user?.firstname} {appState.appSession?.user?.lastname}*!/*/}
                    {/*        /!*</div>*!/*/}
                    {/*        <label tabIndex="0" class="btn btn-ghost btn-circle avatar">*/}
                    {/*            <div class="w-10 rounded-full">*/}
                    {/*                <AccountCircle width="w-10"/>*/}
                    {/*                /!*<img alt="user profile picture" src={appState.appSession?.user?.photo_url}/>*!/*/}
                    {/*            </div>*/}
                    {/*        </label>*/}
                    {/*        <ul tabIndex="0"*/}
                    {/*            class="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52">*/}
                    {/*            /!*<li><a>Settings</a></li>*!/*/}
                    {/*            <li><a>Logout</a></li>*/}
                    {/*        </ul>*/}
                    {/*    </div>*/}
                    {/*</div>*/}
                </Show>
            </nav>

            <main class="">
                <Show when={appState.apolloClient !== null} fallback={<Route/>}>
                    <ApolloProvider client={appState.apolloClient}>
                        <Route/>
                    </ApolloProvider>
                </Show>
            </main>
        </div>
    );
};

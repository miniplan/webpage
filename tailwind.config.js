module.exports = {
    purge: ["./index.html", "./src/**/*.{css,js,ts,jsx,tsx}"],
    darkMode: false, // or 'media' or 'class'
    theme: {
        extend: {},
    },
    variants: {
        extend: {
            backgroundColor: ["disabled"],
            textColor: ["disabled"],
            cursor: ["disabled"],
            boxShadow: ["disabled"]
        }
    },
    plugins: [
        require("daisyui")
    ],
    daisyui: {
        styled: true,
        themes: false,
        rtl: false,
    },

}

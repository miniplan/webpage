FROM nginx:latest
COPY dist/ /usr/share/nginx/html/
ADD public /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
